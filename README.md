# Mariadb import into container

Accessing container

kubectl exec -it <pod name> -- /bin/bash

DB import

mysql -u <user> -p <database-name> < /tmp/drupaldb.sql

#Updating Drupal & modules

There were porblems upgrading drupal 8.7.2 => 8.7.6 to fix that plugin "wikimedia/composer-merge-plugin" (special treatment for the core/ folder when running tests / composer install) was removed.

Links to related topic:
https://www.drupal.org/project/drupal/issues/3020337

https://www.drupal.org/forum/support/upgrading-drupal/2019-01-20/composer-update-issue-867
^Another fix suggested:
1) Downgrade composer to 1.7.2
2) Migrate Drupal to diffrent distribution

https://www.drupal.org/project/drupal/issues/2912387

# Angular nginx config is edited so that Drupal url's would work correctly.

redirects are in configured in angular repo:
nginx/default.conf
  location  /user {
  return 301 /cms/user/$1;
  }
    location  /core {
  return 301 /cms/core/$1;
  }
      location  /modules {
  return 301 /cms/modules/$1;
  }
      location  /profiles {
  return 301 /cms/profiles/$1;
  }
      location  /sites {
  return 301 /cms/sites/$1;
  }
      location  /themes{
  return 301 /cms/themes/$1;
  }
      location  /vendor{
  return 301 /cms/vendor/$1;
  }

  # Drupal db secrets are defined in kubernetes as a secret:

  all Drupal secrets ar used in settings.php file (/var/www/html/cms/sites/default/settings.php)
  Drupal secretes are in "frontend-secrets.yaml" chart together with angular secrets.
  To get container environment secrets Drupal uses php funtions(Check settings.php file).